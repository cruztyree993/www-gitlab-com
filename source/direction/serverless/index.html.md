---
layout: markdown_page
title: "Product Vision - Serverless"
---

- TOC
{:toc}

Serverless computing provides an easy way to build highly scalable applications and services, eliminating the pains of provisioning & maintaining.

Today it's mostly used one-off for e.g. image transformations and ETL, but given the potential and the rise of microservices, it's fully possible to build complex, complete applications on nothing but serverless functions and connected services. Leveraging knative and kubernetes, users will be able to define and manage functions in GitLab. This includes security, logging, scaling, and costs of their serverless implementation for a particular project/group.

## What's next & why
- Add a runtime files to a build context [https://gitlab.com/gitlab-org/gitlabktl/issues/23](https://gitlab.com/gitlab-org/gitlabktl/issues/23)

We currently support building custom runtimes, but runtimes like [Ruby](https://gitlab.com/gitlab-org/serverless/runtimes/ruby) or [NodeJS](https://gitlab.com/gitlab-org/serverless/runtimes/ruby) are just a single file `Dockerfile.template`.

Because it is just a single file, our initial implementation loads this repository to a memory, and generates `Dockerfile` and saves it to disk. We never persist `Dockerfile.template` or anything else from a runtime repository to a disk, therefore these files can not be included in a _context_ (Kaniko / Docker Engine context) before a build starts.

This will allow us to include files in context and lower the barrier of creating new runtimes.

## North Stars

We apply our global product strategy to thinking about the serverless space. We treat the following
principles as our north stars:
- **Multi-Platform Solution** - GitLab will provide an outstanding experience for developers no matter which serverless platform they choose.  
- **Lovable Experience** - Serverless developers will get outsized benefits from using GitLab and will not consider any other application for their serverless needs.


## Stages with serverless focus

There are several stages involved in delivering a comprehensive, quality serverless experience at GitLab. These include, but are not necessarily limited to the following:

- [Configure](/direction/configure/)

## Highlighted epics and issues

There are a few epics and important issues you can check out to see where we're headed.

- [&155](https://gitlab.com/groups/gitlab-org/-/epics/155): Serverless
