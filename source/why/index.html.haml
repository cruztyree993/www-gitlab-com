---
title: Why GitLab?
suppress_header: true
extra_css:
  - auto-devops.css
extra_js:
  - features.js
---

.blank-header
  %img.image-border.image-border-left{ src: "/images/home/icons-pattern-left.svg", alt: "Gitlab hero border pattern left svg" }
  %img.image-border.image-border-right{ src: "/images/home/icons-pattern-right.svg", alt: "Gitlab hero border pattern right svg" }
  .flex-container.flex-column.justify-center.align-center
    %h1 Why GitLab?
    %a.btn.cta-btn.accent{ href: "/free-trial/" } Try GitLab for Free

= partial "includes/home/customer-logos"

.content-container
  .content.tile
    :markdown
      GitLab is a [single application for the entire DevOps lifecycle](/what-is-gitlab/) that allows teams
      to work together better and bring more value to your customers, faster.

      GitLab does this by shortening your DevOps cycle time, bridging silos and stages,
      and taking work out of your hands. It also means a united workflow that reduces friction from traditionally separate activities like application security testing.

  .content.tile
    :markdown
      ## Benefits of shorter cycle time

      A shorter cycle allows you to skate to where the puck is going to be. That means it takes
      less time to go from an idea for a change to actually having that change live in a production
      environment, monitored and ready for scaling.

      ![](https://docs.gitlab.com/ee/img/devops-stages.png)

      By shortening this time, you're able to respond to changing needs from the market faster,
      adjust your long-term plans with feedback you receive on the way, and radically reduce
      engineering risk.

      ### Shorter feedback cycle

      ![](/images/why/puck.png)

      A shorter cycle time allows you to immediately respond to changing needs. Rather than having
      to wait upwards of several months for the DevOps cycle to complete, with a short cycle you're
      able to adjust your plans quickly. With each iteration you complete, you get new opportunities
      to collect feedback. Be that low-level feedback on the performance of your products, or direct
      feedback from your customers. GitLab has monitoring built-in, so no time is wasted on trying
      to get information surfaced.


      ### Reduce engineering risk

      In addition to creating a shorter feedback cycle and being able to respond quicker to changing needs,
      a surprising advantage of shorter cycles is reduced engineering risk. Shorter cycles mean more
      and smaller frequent deploys, which have many advantages:

      - Easier to coordinate and reason about
      - Higher predictability: smaller iterations are easier to estimate than larger ones
      - Better code quality: every small change gets attention, rather than all at once
      - Easier troubleshooting: a smaller deploy introduces less changes that can potentially introduce issues

      ### More secure applications

      Shorter cycle times means feedback on security vulnerabilities is delivered directly to the responsible developer right in their merge request pipeline report. Even dynamic application security testing (DAST), that requires a working application, can be done prior to merging the branch, using GitLab's review app function. Many vulnerabilities can be removed during development without wasting cycles downstream for security teams to vet findings, prioritize and triage and then create tickets to remediate.


      GitLab shortens your cycles by bringing everyone together and taking work out of your hands.

      [Read about Cycle Analytics](/features/cycle-analytics)

  .content.tile
    :markdown
      ## Bridging Silos and Stages

      In GitLab, everyone looks at the same things. There is a single source of truth for every single change, and it is linked automatically to anything relevant. This is because GitLab is a single application. Project management, code review, continuous integration and delivery – and even application security and monitoring are all part of GitLab. This means:

      - No more formal handoffs between teams
      - No need to maintain and secure integration points
      - There is always a clear single source of truth

      We like to think of the DevOps lifecycle becoming a single conversation, where GitLab automatically links
      all relevant information together.

      ![](/images/why/thread2.png)

      And this means one team no longer needs to wait for the handoff of another team. For example:

      - QA can join the conversation earlier in the cycle, potentially catching issues early on and
      reducing bottlenecks and gatekeeping later in the lifecycle.
      - The developer can ping the security pro in a comment to ask for help with vulnerable code with both looking at the same scan results.


      We call this [Concurrent DevOps](/concurrent-devops).

      ![](/images/why/gatek.png)

      [See more advantages of GitLab being a single application](/handbook/product/single-application/)

  .content.tile
    :markdown
      ## Taking work out of your hands

      You're working together in GitLab, everyone is looking at the same data, handoffs are a thing
      of the past and silos are blurring their lines. This is a great way to reduce cycle time,
      but GitLab does more.

      Because GitLab is a complete DevOps platform, delivered as a single application that has everything you need to go from planning to
      shipping to production and even monitoring, you no longer need a complex DevOps tool chain. There is no more need to set up integrations,
      worry about authentication and authorization between the various applications and maintain a slew of apps to manage your DevOps SDLC.

      [See all DevOps stages that GitLab has built-in](/product)

      GitLab will automatically build, test, analyze code quality, do dependency scanning, license compliance,
      container scanning, security testing, monitor and more through Auto DevOps.

      [Auto DevOps](/auto-devops)

  .content.tile
    :markdown
      ## The Goal

      Ultimately, with GitLab, teams are shipping changes continuously:

      - Every change is fully tested and secured
      - For every change, there is a single thread that contains a full audit log of every decision and action
      - Everything is automatically rolled out to staging / production environments
      - A single feedback loop is created, through built-in monitoring and project management tools
      - Everyone is on the same page, always looking at the same data

      From a higher level:

      - Feedback from your systems is in the same place as where code gets written, empowering individual developers to address issues earlier
      - Any market feedback can be addressed immediately, as your cycle time is very low, allowing quick course corrections
      - You have full visibility and control over delivery and quality, as it's all in the same place
      - Everything is audited
