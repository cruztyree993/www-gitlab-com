---
layout: markdown_page
title: "AM.1.01 - Inventory Management Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# AM.1.01 - Inventory Management

## Control Statement

GitLab maintains an inventory of system devices, which is reconciled quarterly.

## Context

The purpose of this control is to ensure we are monitoring the systems in use by GitLab. We can't prove we are protecting all GitLab systems if we don't have an up-to-date inventory of those systems.

## Scope

This control applies to all GitLab endpoint workstations as well as virtual assets within our hosting providers.

## Ownership

* IT Operations owns the workstation assets portion of this control
* Infrastructure owns the system and service portions of this control

## Guidance

The scope of this control is broad by design. Asset inventories are the source of truth for what team-member workstations, systems, and services constitute GitLab as a company. If we want to verify if we are collecting logs on 100% of the systems we are required to collect logs for, this inventory allows us to cross reference the logs we have with all the systems for which these logs should exist.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Inventory Management control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/761).

## Framework Mapping

* ISO
  * A.8.1.1
* PCI
  * 9.6.1
  * 9.7
  * 9.7.1
